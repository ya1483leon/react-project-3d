import React from 'react';
import styled from "styled-components";
import { Canvas } from '@react-three/fiber';
import { OrbitControls } from '@react-three/drei';
import Cube from './Cube';

const Section = styled.div`
  height: 100vh;
  scroll-snap-align: center;
  display: flex;
  justify-content: center;
`;
const Container = styled.div`
  height: 100vh;
  scroll-snap-align: center;
  width: 87.5rem;
  display: flex;
  justify-content: space-between;
`;
const Info = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);

  @media only screen and (max-width: 768px) {
    align-items: center;
    text-align: center;
  }
`;
const Title = styled.h1`
  font-size: clamp(1.25rem, 0.406rem + 4.82vw, 4.625rem);

  @media only screen and (max-width: 768px) {
    font-size: 60px;
  }
`;
const WhatWeDo = styled.div`
  display: flex;
  align-items: center;
  gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
`;
const Line = styled.img`
  height: 5px;
`;
const Subtitle = styled.h2`
  color: #da4ea2;
`;
const Desc = styled.p`
  font-size: clamp(0.625rem, 0.406rem + 1.25vw, 1.5rem);
  color: lightgray;
`;
const Button = styled.button`
  width: clamp(1.25rem, 7.14vw, 6.75rem);
  padding: clamp(0.313rem, 0.234rem + 0.45vw, 0.625rem);
  background-color: #da4ea2;
  color: #FFFFFF;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  font-weight: 500;
`;
const Image = styled.div`
  position: relative;
  flex: 1;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;


const Who = () => {
  return (
    <Section>
      <Container>
        <Image>
        <Canvas camera={{ fov:25, position: [5,5,5] }}>
            <OrbitControls enableZoom={false} autoRotate/>
            <ambientLight intensity={1}/>
            <directionalLight position={[3, 2, 1]} />
            <Cube/>
        </Canvas>
        </Image>
        <Info>
            <Title>Think outside the square space</Title>
            <WhatWeDo>
              <Line src="./img/line.png"/>
              <Subtitle>Who we Are</Subtitle>
            </WhatWeDo>
            <Desc>a creative froup of designers and developers with a passion for the arts.</Desc>
            <Button>See our works</Button>
        </Info>
      </Container>
    </Section>
  )
}

export default Who;