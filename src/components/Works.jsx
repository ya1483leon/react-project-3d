import React, { useState } from 'react';
import styled from "styled-components";
import WebDesign from "./WebDesign";
import ProductDesign from "./ProductDesign";
import Development from "./Development";
import Illustration from "./Illustration";
import SocialMedia from "./SocialMedia";

const data = [
  "Web Design",
  "Development",
  "Illustration",
  "Product Design",
  "Social Media",
];

const Section = styled.div`
  height: 100vh;
  scroll-snap-align: center;
  display: flex;
  flex-direction: center;
  position: relative;
  color: #000000;
  font-size: clamp(0.625rem, 0.563rem + 0.36vw, 0.875rem);
  font-weight: 300;
`;
const Container = styled.div`
  width: 87.5rem;
  display: flex;
  justify-content: space-between;

  @media only screen and (max-width: 768px) {
    width: 100%;
    flex-direction: column;
  }
`;
const Info = styled.div`
  flex: 1;
  align-items: center;
  display: flex;
  justify-content: center;

  @media only screen and (max-width: 768px) {
    padding: 20px;
  }
`;
const List = styled.ul`
  list-style: none;
  display: flex;
  flex-direction: column;
  gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
`;
const ListItem = styled.li`
  font-size: clamp(2.5rem, 1.563rem + 5.36vw, 6rem);
  font-weight: bold;
  cursor: pointer;
  color: transparent;
  position: relative;
  -webkit-text-stroke: 1px white;

  @media only screen and (max-width: 768px) {
    font-size: 24px;
    color: #FFFFFF;
    -webkit-text-stroke: 0 white;
  }

  ::after{
    content: "${(props) => props.text}";
    position: absolute;
    top: 0;
    left: 0;
    width: 0;
    overflow: hidden;
    white-space: nowrap;
  }

  &:hover{
    ::after{
      content: "${(props) => props.text}";
      position: absolute;
      top: 0;
      left: 0;
      color: rebeccapurple;
      animation: moveText .5s linear both;

      @keyframes moveText {
        to{
          width: 100%;
        }
      }
    }
  }
`;
const Image = styled.div`
  height: 100vh;
  scroll-snap-align: center;
`;

const Works = () => {
  const [work, setWork] = useState("Web Design");
  return (
    <Section>
      <Container>
      <Info>
        <List>
          {data.map((item) => (
            <ListItem key={item} text={item} onClick={() => setWork(item)}>
              {item}
            </ListItem>
          ))}
        </List>
      </Info>
      <Image>
        {work === "Web Design" ? (
        <WebDesign/>
        ) : work === "Development" ? (
        <Development/>
        ) : work === "Illustration" ? (
          <Illustration/>)
          : work === "Product Design" ? (
            <ProductDesign/>)
          : (<SocialMedia/>
      )}
      </Image>
      </Container>
    </Section>
  )
}

export default Works