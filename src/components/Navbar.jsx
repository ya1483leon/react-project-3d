import React from 'react'
import styled from "styled-components"

const Section = styled.div`
    display: flex;
    justify-content: center;

    @media only screen and (max-width: 768px) {
        width: 100%;
    }
`;
const Container = styled.div`
    width: 87.5rem;
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 1rem 0;

    @media only screen and (max-width: 768px) {
        width: 100%;
        padding: 10px;
    }
`;
const Links = styled.div`
    display: flex;
    align-items: center;
    gap: clamp(1.25rem, 0.781rem + 2.68vw, 3.125rem);
    list-style: none;
`;
const Logo = styled.img`
    height: clamp(1.25rem, 0.781rem + 2.68vw, 3.125rem)
`;
const List = styled.div`
    display: flex;
    gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);

    @media only screen and (max-width: 768px) {
        display: none;
    }
`;
const ListItem = styled.li`
    cursor: pointer;
`;
const Icon = styled.img`
    width: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem); 
    cursor: pointer;
`;
const Icons = styled.div`
    display: flex;
    align-items: center;
    gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
    list-style: none;
`;
const Button = styled.button`
    width: clamp(1.25rem, 7.14vw, 6.25rem);
    padding: clamp(0.313rem, 0.234rem + 0.45vw, 0.625rem);
    background-color: #da4ea2;
    color: #FFFFFF;
    cursor: pointer;
    border: none;
    border-radius: 5px;
`;

const Navbar = () => {
  return (
    <Section>
        <Container>
            <Links>
            <Logo src="./img/logo.png"/>
            <List>
                <ListItem>Home</ListItem>
                <ListItem>Studio</ListItem>
                <ListItem>Works</ListItem>
                <ListItem>Contact</ListItem>
            </List>
            </Links>
            <Icons>
                <Icon src="./img/search.png"/>
                <Button>Hire Now</Button>
            </Icons>
        </Container>
    </Section>
  )
}

export default Navbar