import React, { useRef, useState } from 'react';
import emailjs from '@emailjs/browser';
import styled from "styled-components";
import Map from "./Map";
import { Annotation } from "react-simple-maps"

const Section = styled.div`
  height: 100vh;
  scroll-snap-align: center;
`;
const Container = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  justify-content: space-between;
  gap: clamp(1.25rem, 0.781rem + 2.68vw, 3.125rem);
`;
const Info = styled.div`
  flex: 1;
  display: flex;
  align-items: center;
  justify-content: flex-end;

  @media only screen and (max-width: 768px) {
    justify-content: center;
  }
`;
const Title = styled.h1`
  font-weight: 200;
`;
const Form = styled.form`
  width: clamp(6.25rem, 35.71vw, 31.25rem);
  display: flex;
  flex-direction: column;
  gap: clamp(0.625rem, 0.391rem + 1.34vw, 1.563rem);

  @media only screen and (max-width: 768px) {
    width: 280px;
  }
`;
const Input = styled.input`
  padding: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
  background-color: #e8e6e6;
  border: none;
  border-radius: 5px;
`;
const TextArea = styled.textarea`
  border: none;
  border-radius: 5px;
  background-color: #e8e6e6;
  padding: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
`;
const Button = styled.button`
  background-color: #da4ea2;
  color: #FFFFFF;
  border-radius: 5px;
  border: none;
  font-weight: bold;
  cursor: pointer;
  padding: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
`;
const Image = styled.div`
  flex: 1;

  @media only screen and (max-width: 768px) {
    display: none;
  }
`;


const Contact = () => {
  const ref = useRef();
  const [success, setSuccess] = useState(null);


  const handleSubmit = e => {
    e.preventDefault()

    emailjs.sendForm('service_dwxidhm', 'template_bdqshgo', ref.current, 'vYY6wza0L2X1mJZiD')
    .then((result) => {
        console.log(result.text);
        setSuccess(true);
    }, (error) => {
        console.log(error.text);
        setSuccess(false);
    });

  }
  return (
    <Section>
      <Container>
        <Info>
            <Form ref={ref} onSubmit={handleSubmit}>
              <Title>Contact Us</Title>
              <Input placeholder='Name' name='name'/>
              <Input placeholder='e-mail' name='email'/>
              <TextArea placeholder='Write your message' name='message' rows={10}/>
              <Button type="submit">Send</Button>
              {success && "Your message has been sent. We'll get back to you soon :)"}
            </Form>
        </Info>
        <Image>
            <Map/>
        </Image>
      </Container>
    </Section>
  )
}

export default Contact;