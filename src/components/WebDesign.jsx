import React from 'react';
import { OrbitControls, Stage } from '@react-three/drei';
import { Canvas } from '@react-three/fiber';
import Chair from './Chair';
import styled from "styled-components";

const Desc = styled.div`
  width: clamp(2.5rem, 14.29vw, 12.5rem);
  height: clamp(2.5rem, 1.563rem + 5.36vw, 6.25rem);
  padding: clamp(0.625rem, 0.391rem + 1.34vw, 1.563rem);
  background-color: #FFFFFF;
  border-radius: 10px;
  position: absolute;
  top: 100px;
  right: 100px;

  @media only screen and (max-width: 768px) {
    top: 0;
    right: 0;
    left: 0;
    bottom: 0;
    margin: auto;
  }
`;

const WebDesign = () => {
  return (
    <>
      <Canvas>
        <Stage environment="city" intensity={0.6}>
          <Chair/>
        </Stage>
        <OrbitControls enableZoom={false} autoRotate/>
      </Canvas>
      <Desc>
        We design products with a strong focus on both world class design and ensuring your product is a market success.
      </Desc>
    </>
  )
}

export default WebDesign;