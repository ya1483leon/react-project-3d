import React from 'react';
import styled from "styled-components";
import Navbar from './Navbar';
import { Canvas } from '@react-three/fiber';
import { OrbitControls, Sphere, MeshDistortMaterial } from '@react-three/drei';

const Section = styled.div`
  height: 100vh;
  scroll-snap-align: center;
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  @media only screen and (max-width: 768px) {
    height: 200vh;
  }
`;
const Container = styled.div`
  height: 100%;
  scroll-snap-align: center;
  width: 87.5rem;
  display: flex;
  justify-content: space-between;

  @media only screen and (max-width: 768px) {
    width: 100%;
    flex-direction: column;
    justify-content: center;
    align-items: center;
  }
`;
const Info = styled.div`
  flex: 2;
  display: flex;
  flex-direction: column;
  justify-content: center;
  gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);

  @media only screen and (max-width: 768px) {
    flex: 1;
    align-items: center;
  }
`;
const Title = styled.h1`
  font-size: clamp(1.25rem, 0.406rem + 4.82vw, 4.625rem);

  @media only screen and (max-width: 768px) {
    text-align: center;
  }

`;
const WhatWeDo = styled.div`
  display: flex;
  align-items: center;
  gap: clamp(0.625rem, 0.469rem + 0.89vw, 1.25rem);
`;
const Line = styled.img`
  height: 5px;
`;
const Subtitle = styled.h2`
  color: #da4ea2;
`;
const Desc = styled.p`
  font-size: clamp(0.625rem, 0.406rem + 1.25vw, 1.5rem);
  color: lightgray;

  @media only screen and (max-width: 768px) {
    padding: 20px;
    text-align: center;
  }
`;
const Button = styled.button`
  width: clamp(1.25rem, 7.14vw, 6.25rem);
  padding: clamp(0.313rem, 0.234rem + 0.45vw, 0.625rem);
  background-color: #da4ea2;
  color: #FFFFFF;
  cursor: pointer;
  border: none;
  border-radius: 5px;
  font-weight: 500;
`;
const Image = styled.div`
  position: relative;
  flex: 3;

  @media only screen and (max-width: 768px) {
    flex: 1;
    width: 100%;
  }
`;
const Img = styled.img`
  width: clamp(6.25rem, -4.688rem + 62.5vw, 50rem);
  height: clamp(5rem, -3.125rem + 46.43vw, 37.5rem);
  object-fit: contain;
  position: absolute;
  top: 0;
  bottom: 0;
  left: 0;
  right: 0;
  margin: auto;
  animation: animate 2s infinite ease alternate;

  @keyframes animate {
    to{
      transform: translateY(1.5rem);
    }
  }

  @media only screen and (max-width: 768px) {
    width: 300px;
    height: 300px;
  }
`;

const Hero = () => {
  return (
    <Section>
      <Navbar/>
      <Container>
        <Info>
            <Title>Think. Make. Solve</Title>
            <WhatWeDo>
              <Line src="./img/line.png"/>
              <Subtitle>What we Do</Subtitle>
            </WhatWeDo>
            <Desc>we enjoy creating delightful, human-centered digital experiences</Desc>
            <Button>Learn More</Button>
        </Info>
        <Image>
        <Canvas>
            <OrbitControls enableZoom={false} autoRotate/>
            <ambientLight intensity={1}/>
            <directionalLight position={[3, 2, 1]} />
            <Sphere args={[1, 100, 200]} scale={1.5}>
            <MeshDistortMaterial
             color="#3d1c56" 
             attach="material" 
             distort={0.5} 
             speed={2}
             />
             </Sphere>
        </Canvas>
          <Img src="./img/moon.png"/>
        </Image>
      </Container>
    </Section>
  )
}

export default Hero